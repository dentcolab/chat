use futures_util::StreamExt;
use std::{
	collections::HashMap,
	env,
	io::Error,
	net::SocketAddr,
	string::String,
	sync::{Arc, Mutex},
};
use tokio::net::{TcpListener, TcpStream};

struct Client {
	pub addr: SocketAddr,
	pub read: futures_util::stream::SplitStream<
		tokio_tungstenite::WebSocketStream<tokio::net::TcpStream>,
	>,
	pub write: futures_util::stream::SplitSink<
		tokio_tungstenite::WebSocketStream<tokio::net::TcpStream>,
		tokio_tungstenite::tungstenite::Message,
	>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
	let addr = env::args()
		.nth(1)
		.unwrap_or_else(|| "127.0.0.1:8080".to_string());

	let clients: Arc<Mutex<HashMap<String, Vec<Client>>>> = Arc::new(Mutex::new(HashMap::new()));
	// Create the event loop and TCP listener we'll accept connections on.
	let listener = TcpListener::bind(&addr)
		.await
		.expect("Failed to create TCP listener!");

	while let Ok((stream, addr)) = listener.accept().await {
		tokio::spawn(accept_connection(clients.clone(), stream, addr));
	}

	Ok(())
}

async fn accept_connection(
	clients: Arc<Mutex<HashMap<String, Vec<Client>>>>,
	stream: TcpStream,
	addr: SocketAddr,
) {
	println!("New connection {}", addr);
	let ws_stream = tokio_tungstenite::accept_async(stream).await.expect("");
	let (write, read) = ws_stream.split();
	clients.lock().unwrap().insert(
		String::from("id"),
		vec![Client {
			addr: addr,
			read: read,
			write: write,
		}],
	);
}
